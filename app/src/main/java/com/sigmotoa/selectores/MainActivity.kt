package com.sigmotoa.selectores

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*

var rab1:RadioButton?=null
var rab2:RadioButton?=null
var rab3:RadioButton?=null
var rab4:RadioButton?=null


class MainActivity : AppCompatActivity(), RadioGroup.OnCheckedChangeListener  {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val rtb:RatingBar=findViewById(R.id.RatingBar)
        rtb.setOnRatingBarChangeListener { ratingBar, fl, b -> msg("rating $fl") }

        var rg:RadioGroup = findViewById(R.id.RadioGroup)
        rab1 = findViewById(R.id.Radio1)
        rab2 = findViewById(R.id.Radio2)
        rab3 = findViewById(R.id.Radio3)
        rab4 = findViewById(R.id.Radio4)

        rg?.setOnCheckedChangeListener(this)
        val box1:CheckBox = findViewById(R.id.CheckB1)
        val box2:CheckBox = findViewById(R.id.CheckB2)

        box1.setOnClickListener {
            if(box1.isChecked)
            {
                msg("CheckBox1")
            }
        }
        box2.setOnClickListener {
            if (box2.isChecked)
            {
                msg("CheckBox2")
            }
        }
    }

    fun msg (number:String){

        val tv:TextView = findViewById(R.id.Selectores)
        tv.setText("Pressed $number")
        var msg=Toast.makeText(this, "Option $number", Toast.LENGTH_SHORT)
            msg.setGravity(Gravity.TOP,0,0)
            msg.show()
    }
    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton){
            val checked = view.isChecked
            when(view.id){
                R.id.Radio5-> if (checked)
                {msg("Option 5")}
                R.id.Radio6->if (checked)
                {msg("Option 6")}

            }
        }
    }

    override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {

        when(p1){
            rab1?.id ->msg("1")
            rab2?.id ->msg("2")
            rab3?.id ->msg("3")
            rab4?.id ->msg("4")
        }
    }



}







